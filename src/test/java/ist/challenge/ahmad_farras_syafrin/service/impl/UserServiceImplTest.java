package ist.challenge.ahmad_farras_syafrin.service.impl;

import ist.challenge.ahmad_farras_syafrin.entity.User;
import ist.challenge.ahmad_farras_syafrin.error.NoChangeException;
import ist.challenge.ahmad_farras_syafrin.error.NotFoundExeption;
import ist.challenge.ahmad_farras_syafrin.payload.UserReq;
import ist.challenge.ahmad_farras_syafrin.repository.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.dao.DataIntegrityViolationException;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserServiceImpl userService;
    private AutoCloseable autoCloseable;

    @BeforeEach
    void setup(){
        autoCloseable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        autoCloseable.close();
    }

    @Test
    void registerSuccess() {
        //given
        UserReq userReq = new UserReq("Farras", "123Abc");

        //when
        userService.register(userReq);

        //then
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User userCaptured = userArgumentCaptor.getValue();

        assertThat(userCaptured.getUsername()).isEqualTo(userReq.getUsername());
        assertThat(userCaptured.getPassword()).isEqualTo(userReq.getPassword());
    }

    @Test
    void registerFailedCauseUsernameAlreadyExist() {
        //given
        UserReq userReq = new UserReq("Farras", "123Abc");

        User user = new User("Farras", "123Abc");
        given(userRepository.findByUsername(user.getUsername())).willReturn(java.util.Optional.of(user));

        //when

        //then
        assertThatThrownBy(() -> userService.register(userReq))
                .isInstanceOf(DataIntegrityViolationException.class)
                .hasMessageContaining("Username sudah terpakai");
    }

    @Test
    void updateSuccess() throws NotFoundExeption, NoChangeException {
        //given
        UserReq userReq = new UserReq("Farras", "tes123");
        User user = new User();
        user.setId(1L);
        user.setUsername("Farras");
        user.setPassword("123Abc");

        given(userRepository.findById(1L)).willReturn(java.util.Optional.of(user));
        given(userRepository.findByUsername(userReq.getUsername())).willReturn(java.util.Optional.of(user));

        //when
        userService.update(1, userReq);

        //then
        ArgumentCaptor<User> userArgumentCaptor = ArgumentCaptor.forClass(User.class);
        verify(userRepository).save(userArgumentCaptor.capture());
        User userCaptured = userArgumentCaptor.getValue();

        assertThat(userCaptured.getUsername()).isEqualTo(userReq.getUsername());
        assertThat(userCaptured.getPassword()).isEqualTo(userReq.getPassword());
    }

    @Test
    void updateFailedCauseUsernameAlreadyExist() {
        //given
        UserReq userReq = new UserReq("Farras", "tes123");
        User user = new User();
        user.setId(1L);
        user.setUsername("Budi");
        user.setPassword("123Abc");

        User user2 = new User();
        user2.setId(2L);
        user2.setUsername("Farras");
        user2.setPassword("123Abc");

        given(userRepository.findById(1L)).willReturn(java.util.Optional.of(user));
        given(userRepository.findByUsername(userReq.getUsername())).willReturn(java.util.Optional.of(user2));

        //when

        //then
        assertThatThrownBy(() -> userService.update(1, userReq))
                .isInstanceOf(DataIntegrityViolationException.class)
                .hasMessageContaining("Username sudah terpakai");
    }

    @Test
    void updateFailedCauseNoChange() {
        //given
        UserReq userReq = new UserReq("Farras", "tes123");
        User user = new User();
        user.setId(1L);
        user.setUsername("Farras");
        user.setPassword("tes123");

        given(userRepository.findById(1L)).willReturn(java.util.Optional.of(user));
        given(userRepository.findByUsername(userReq.getUsername())).willReturn(java.util.Optional.of(user));

        //when

        //then
        assertThatThrownBy(() -> userService.update(1, userReq))
                .isInstanceOf(NoChangeException.class)
                .hasMessageContaining("Password tidak boleh sama dengan password sebelumnya");
    }
}