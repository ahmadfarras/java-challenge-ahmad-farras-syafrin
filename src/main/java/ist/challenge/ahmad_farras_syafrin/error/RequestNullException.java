package ist.challenge.ahmad_farras_syafrin.error;

public class RequestNullException extends Exception{
    public RequestNullException() {
    }

    public RequestNullException(String msg) {
        super(msg);
    }
}
