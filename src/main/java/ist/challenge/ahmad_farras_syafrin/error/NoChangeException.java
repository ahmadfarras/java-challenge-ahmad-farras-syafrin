package ist.challenge.ahmad_farras_syafrin.error;

public class NoChangeException extends Exception{
    public NoChangeException() {
    }

    public NoChangeException(String msg) {
        super(msg);
    }
}
