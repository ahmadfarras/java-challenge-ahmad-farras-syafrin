package ist.challenge.ahmad_farras_syafrin.error;

public class NotFoundExeption extends Exception{

    public NotFoundExeption() {
    }

    public NotFoundExeption(String msg) {
        super(msg);
    }
}
