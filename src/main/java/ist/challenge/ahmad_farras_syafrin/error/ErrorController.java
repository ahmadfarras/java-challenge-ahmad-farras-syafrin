package ist.challenge.ahmad_farras_syafrin.error;

import ist.challenge.ahmad_farras_syafrin.payload.WebResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@RestControllerAdvice
public class ErrorController {
    Logger logger = LoggerFactory.getLogger(this.getClass());

    @ExceptionHandler(DataIntegrityViolationException.class)
    public ResponseEntity<Map<String, Object>> validationException(HttpServletRequest req, DataIntegrityViolationException ex) {
        logger.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new WebResponse().badRequest("FAILED!", HttpStatus.CONFLICT, ex.getMessage());
    }

    @ExceptionHandler(NotFoundExeption.class)
    public ResponseEntity<Map<String, Object>> NotFoundException(HttpServletRequest req, NotFoundExeption ex) {
        logger.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new WebResponse().badRequest("NOT FOUND!", HttpStatus.NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler(NoChangeException.class)
    public ResponseEntity<Map<String, Object>> NoChangeException(HttpServletRequest req, NoChangeException ex) {
        logger.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new WebResponse().badRequest("NO CHANGE!", HttpStatus.BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(RequestNullException.class)
    public ResponseEntity<Map<String, Object>> RequestNullException(HttpServletRequest req, RequestNullException ex) {
        logger.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new WebResponse().badRequest("BLANK FIELD!", HttpStatus.BAD_REQUEST, ex.getMessage());
    }

    @ExceptionHandler(UsernameNotFoundException.class)
    public ResponseEntity<Map<String, Object>> UsernameNotFoundException(HttpServletRequest req, UsernameNotFoundException ex) {
        logger.error("Request: " + req.getRequestURL() + " raised " + ex);
        return new WebResponse().badRequest("NOT FOUND!", HttpStatus.BAD_REQUEST, ex.getMessage());
    }
}
