package ist.challenge.ahmad_farras_syafrin.controller;

import ist.challenge.ahmad_farras_syafrin.error.RequestNullException;
import ist.challenge.ahmad_farras_syafrin.payload.LoginRes;
import ist.challenge.ahmad_farras_syafrin.payload.UserReq;
import ist.challenge.ahmad_farras_syafrin.payload.WebResponse;
import ist.challenge.ahmad_farras_syafrin.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;

@RestController
@RequestMapping(value = "/auth")
public class AuthController {

    @Autowired
    LoginService loginService;

    @PostMapping(
            produces = "application/json",
            consumes = "application/json"
    )
    private ResponseEntity<Map<String, Object>> login(@Valid @RequestBody UserReq payload) throws RequestNullException {
        LoginRes loginResponse = loginService.authenticate(payload);
        return new WebResponse().success("Sukses Login", HttpStatus.OK, loginResponse);
    }
}
