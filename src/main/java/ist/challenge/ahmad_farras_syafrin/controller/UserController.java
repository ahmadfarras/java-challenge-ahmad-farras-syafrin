package ist.challenge.ahmad_farras_syafrin.controller;

import ist.challenge.ahmad_farras_syafrin.error.NoChangeException;
import ist.challenge.ahmad_farras_syafrin.error.NotFoundExeption;
import ist.challenge.ahmad_farras_syafrin.payload.*;
import ist.challenge.ahmad_farras_syafrin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.text.ParseException;
import java.util.Map;

@RestController
@RequestMapping(value = "/user")
public class UserController {

    @Autowired
    UserService userService;

    @PostMapping(
            value = "/register",
            produces = "application/json",
            consumes = "application/json"
    )
    private ResponseEntity<Map<String, Object>> register(@Valid @RequestBody UserReq payload){
        userService.register(payload);
        return new WebResponse().success("success", HttpStatus.CREATED, null);
    }

    @GetMapping(
            value = "/{id}",
            produces = "application/json"
    )
    private ResponseEntity<Map<String, Object>> get(@Valid @PathVariable("id") Integer id) throws NotFoundExeption {
        UserRes userRes = userService.get(id);
        return new WebResponse().success("success", HttpStatus.OK, userRes);
    }

    @PutMapping(
            value = "/{id}",
            produces = "application/json",
            consumes = "application/json"
    )
    private ResponseEntity<Map<String, Object>> update(@PathVariable("id") Integer id, @Valid @RequestBody UserReq payload) throws NotFoundExeption, NoChangeException {
        userService.update(id, payload);
        return new WebResponse().success("success", HttpStatus.CREATED, null);
    }

    @GetMapping(
            value = "/list",
            produces = "application/json"
    )
    private ResponseEntity<Map<String, Object>> list(@RequestParam(value = "size", defaultValue = "10") Integer size,
                                                     @RequestParam(value = "page", defaultValue = "0") Integer page,
                                                     @RequestParam(value = "search", required = false) String search){
        ListReq listRequest = new ListReq(size, page, search);
        ListRes listRes = userService.list(listRequest);
        return new WebResponse().success("success", HttpStatus.OK, listRes);
    }
}
