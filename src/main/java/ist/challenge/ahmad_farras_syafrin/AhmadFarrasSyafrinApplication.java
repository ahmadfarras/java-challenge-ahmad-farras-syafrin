package ist.challenge.ahmad_farras_syafrin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AhmadFarrasSyafrinApplication {

	public static void main(String[] args) {
		SpringApplication.run(AhmadFarrasSyafrinApplication.class, args);
	}

}
