package ist.challenge.ahmad_farras_syafrin.service;

import ist.challenge.ahmad_farras_syafrin.error.RequestNullException;
import ist.challenge.ahmad_farras_syafrin.payload.LoginRes;
import ist.challenge.ahmad_farras_syafrin.payload.UserReq;

public interface LoginService {
    LoginRes authenticate(UserReq userReq) throws RequestNullException;
    boolean isAuthenticated(String username, String password);
}
