package ist.challenge.ahmad_farras_syafrin.service.impl;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import ist.challenge.ahmad_farras_syafrin.entity.User;
import ist.challenge.ahmad_farras_syafrin.error.RequestNullException;
import ist.challenge.ahmad_farras_syafrin.payload.LoginRes;
import ist.challenge.ahmad_farras_syafrin.payload.UserReq;
import ist.challenge.ahmad_farras_syafrin.repository.UserRepository;
import ist.challenge.ahmad_farras_syafrin.service.LoginService;
import org.bouncycastle.util.encoders.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LoginServiceImpl implements LoginService {

    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    UserRepository userRepository;

    @Override
    public LoginRes authenticate(UserReq userReq) throws RequestNullException {
        LoginRes loginResponse = new LoginRes();

        if (userReq.getUsername() == null || userReq.getUsername().isBlank() || userReq.getPassword() == null || userReq.getPassword().isBlank()){
            throw new RequestNullException("Username dan / atau password kosong");
        }

        Optional<User> checkPengguna = userRepository.findByUsernameAndPassword(userReq.getUsername(),  userReq.getPassword());
        if (checkPengguna.isEmpty()){
            throw new UsernameNotFoundException("Username / password salah!");
        }


        Map<String, Object> header = new HashMap<>();
        header.put("typ", "JWT");

        Claims c = Jwts.claims();

        ArrayList<String> audiences = new ArrayList<>();
        Collections.addAll(audiences, "infosysjwt");
        c.put("aud", audiences);
        c.put("user_name", userReq.getUsername());
        ArrayList<String> scopes = new ArrayList<>();
        Collections.addAll(scopes, "read", "write","trust");
        c.put("scope", scopes);
        ArrayList<String> authorities = new ArrayList<>();
        c.put("authorities", authorities);
        c.put("client_id", "client");

        String token = Jwts.builder().setClaims(c)
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .setHeader(header)
                .signWith(SignatureAlgorithm.HS256, Base64.toBase64String(secret.getBytes())).compact();

        loginResponse.setAccessToken(token);

        return loginResponse;
    }

    @Override
    public boolean isAuthenticated(String username, String password) {
        Optional<User> checkPengguna = userRepository.findByUsernameAndPassword(username, password);
        return checkPengguna.isPresent();
    }
}
