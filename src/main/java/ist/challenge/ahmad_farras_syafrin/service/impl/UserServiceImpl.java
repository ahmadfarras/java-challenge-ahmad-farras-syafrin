package ist.challenge.ahmad_farras_syafrin.service.impl;

import ist.challenge.ahmad_farras_syafrin.entity.User;
import ist.challenge.ahmad_farras_syafrin.error.NoChangeException;
import ist.challenge.ahmad_farras_syafrin.error.NotFoundExeption;
import ist.challenge.ahmad_farras_syafrin.payload.ListReq;
import ist.challenge.ahmad_farras_syafrin.payload.ListRes;
import ist.challenge.ahmad_farras_syafrin.payload.UserReq;
import ist.challenge.ahmad_farras_syafrin.payload.UserRes;
import ist.challenge.ahmad_farras_syafrin.repository.UserRepository;
import ist.challenge.ahmad_farras_syafrin.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Override
    public void register(UserReq userReq){
        Optional<User> findUser = userRepository.findByUsername(userReq.getUsername());
        if (findUser.isPresent()){
            throw new DataIntegrityViolationException("Username sudah terpakai");
        }

        userRepository.save(new User(userReq.getUsername(), userReq.getPassword()));
    }

    @Override
    public UserRes get(Integer id) throws NotFoundExeption {
        Optional<User> findUser = userRepository.findById(id.longValue());
        if (findUser.isEmpty()){
            throw new NotFoundExeption("User tidak ditemukan!");
        }

        return convertUserRes(findUser.get());
    }

    @Override
    public void update(Integer id, UserReq userReq) throws NotFoundExeption, NoChangeException {
        Optional<User> findUser = userRepository.findById(id.longValue());
        if (findUser.isEmpty()){
            throw new NotFoundExeption("User tidak ditemukan!");
        }

        User user = findUser.get();
        Optional<User> checkUser = userRepository.findByUsername(userReq.getUsername());
        if (checkUser.isPresent()){
            if (!checkUser.get().getId().equals(id.longValue())){
                throw new DataIntegrityViolationException("Username sudah terpakai");
            }
        }
        if (user.getPassword().equals(userReq.getPassword())){
            throw new NoChangeException("Password tidak boleh sama dengan password sebelumnya");
        }

        user.setUsername(userReq.getUsername());
        user.setPassword(userReq.getPassword());
        userRepository.save(user);
    }

    @Override
    public ListRes list(ListReq listReq) {
        Page<User> userPage;
        if (listReq.getSearch() != null && !listReq.getSearch().equals(" ")){
            userPage = userRepository.findAllByUsernameLike("%"+listReq.getSearch()+"%", PageRequest.of(listReq.getPage(), listReq.getSize()));
        }else{
            userPage = userRepository.findAll(PageRequest.of(listReq.getPage(), listReq.getSize()));
        }

        List<User> userList = userPage.get().collect(Collectors.toList());
        List<UserRes> userResList = new ArrayList<>();
        userList.forEach(it ->
                userResList.add(convertUserRes(it))
        );

        return new ListRes(userResList, userPage.getTotalPages(), userPage.getTotalElements());
    }

    UserRes convertUserRes(User user){
        return new UserRes(
                user.getId(),
                user.getUsername(),
                user.getPassword()
        );
    }
}
