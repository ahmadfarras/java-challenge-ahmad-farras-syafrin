package ist.challenge.ahmad_farras_syafrin.service;

import ist.challenge.ahmad_farras_syafrin.error.NoChangeException;
import ist.challenge.ahmad_farras_syafrin.error.NotFoundExeption;
import ist.challenge.ahmad_farras_syafrin.payload.ListReq;
import ist.challenge.ahmad_farras_syafrin.payload.ListRes;
import ist.challenge.ahmad_farras_syafrin.payload.UserReq;
import ist.challenge.ahmad_farras_syafrin.payload.UserRes;

public interface UserService {
    void register(UserReq userReq);
    UserRes get(Integer id) throws NotFoundExeption;
    void update(Integer id, UserReq userReq) throws NotFoundExeption, NoChangeException;
    ListRes list(ListReq listReq);
}
