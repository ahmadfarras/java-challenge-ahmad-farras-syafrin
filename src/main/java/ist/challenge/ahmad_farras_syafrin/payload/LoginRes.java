package ist.challenge.ahmad_farras_syafrin.payload;

public class LoginRes {
    private String accessToken;

    public LoginRes() {
    }

    public LoginRes(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
}
