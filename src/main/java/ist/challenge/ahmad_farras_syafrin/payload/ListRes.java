package ist.challenge.ahmad_farras_syafrin.payload;

import java.util.List;

public class ListRes {
    private List<?> list;
    private Integer totalPage;
    private Long totalSize;

    public ListRes() {
    }

    public ListRes(List<?> list, Integer totalPage, Long totalSize) {
        this.list = list;
        this.totalPage = totalPage;
        this.totalSize = totalSize;
    }

    public List<?> getList() {
        return list;
    }

    public void setList(List<?> list) {
        this.list = list;
    }

    public Integer getTotalPage() {
        return totalPage;
    }

    public void setTotalPage(Integer totalPage) {
        this.totalPage = totalPage;
    }

    public Long getTotalSize() {
        return totalSize;
    }

    public void setTotalSize(Long totalSize) {
        this.totalSize = totalSize;
    }
}
