package ist.challenge.ahmad_farras_syafrin.payload;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

public class WebResponse {
    public ResponseEntity<Map<String, Object>> success(String status, HttpStatus enumCode, Object data){
        Map<String, Object> res = new HashMap<>();
        res.put("code", enumCode.value());
        res.put("status", status);
        res.put("data", data);
        return new ResponseEntity<>(res, enumCode);
    }

    public ResponseEntity<Map<String, Object>> badRequest(String status, HttpStatus enumCode, String message){
        Map<String, Object> res = new HashMap<>();
        res.put("code", enumCode.value());
        res.put("status", status);
        res.put("message", message);
        return new ResponseEntity<>(res, enumCode);
    }

    public ResponseEntity<Map<String, Object>> error(String status, int code, String message){
        Map<String, Object> res = new HashMap<>();
        res.put("code", code);
        res.put("status", status);
        res.put("message", message);
        return new ResponseEntity<>(res, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
