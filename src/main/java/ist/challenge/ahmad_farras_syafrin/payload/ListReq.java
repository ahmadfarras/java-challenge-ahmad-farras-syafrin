package ist.challenge.ahmad_farras_syafrin.payload;

public class ListReq {
    private Integer size;
    private Integer page;
    private String search;

    public ListReq(Integer size, Integer page) {
        this.size = size;
        this.page = page;
    }

    public ListReq(Integer size, Integer page, String search) {
        this.size = size;
        this.page = page;
        this.search = search;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
