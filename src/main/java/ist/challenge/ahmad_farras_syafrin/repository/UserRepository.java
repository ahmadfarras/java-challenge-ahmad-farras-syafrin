package ist.challenge.ahmad_farras_syafrin.repository;

import ist.challenge.ahmad_farras_syafrin.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByUsername(String username);
    Optional<User> findByUsernameAndPassword(String username, String password);
    Page<User> findAllByUsernameLike(String username, Pageable pageable);
}
