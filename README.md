# API DOC

## Description
Base untuk test login sederhana. Project ini menggunakan maven dan springboot java.

## Build application
Simply run command `mvn install` on your terminal. You can also run `mvn clean install`.
The build will be stored in `/build/libs/` in your project directory.

## Run Application
To run manually in your local, run `mvn spring-boot:run` and navigate to `http://localhost:8080/user-service`.

## API

### REGISTER
Request:
- Method: POST
- Endpoint: `localhost:8080/user-service/user/register`
- Header: 
    - Content-Type: application/json
    - Accept: application/json
- Body:
```json
{
    "username":"infosys",
    "password":"123Abc"
}
```
- Response:
```json
{
    "code": 201,
    "data": null,
    "status": "success"
}
```

### LOGIN
Request:
- Method: POST
- Endpoint: `localhost:8080/user-service/auth`
- Header: 
    - Content-Type: application/json
    - Accept: application/json
- Body:
```json
{
    "username":"infosys",
    "password":"123Abc"
}
```
- Response:
```json
{
    "code": 200,
    "data": {
        "accessToken": "blabla"
    },
    "status": "Sukses Login"
}
```

### LIST
Request:
- Method: GET
- Endpoint: `localhost:8080/user-service/user/list`
- Header: 
    - Accept: application/json
- Authorization:
    - Bearer Token
- Query Param:
    - size: Integer,
    - page: Integer
- Response:
```json
{
    "code": 200,
    "data": {
        "list": [
            {
                "id": 1,
                "username": "inf",
                "password": "abc"
            }
        ],
        "totalPage": 1,
        "totalSize": 1
    },
    "status": "success"
}
```

### GET
Request:
- Method: GET
- Endpoint: `localhost:8080/user-service/user/{id}`
- Header: 
    - Accept: application/json
- Authorization:
    - Bearer Token
- Response:
```json
{
    "code": 200,
    "data": {
        "id": 1,
        "username": "inf",
        "password": "abc"
    },
    "status": "success"
}
```

### UPDATE
Request:
- Method: PUT
- Endpoint: `localhost:8080/user-service/user/{id}`
- Header: 
    - Content-Type: application/json
    - Accept: application/json
- Authorization:
    - Bearer Token
- Body:
```json
{
    "username":"infosys",
    "password":"123Abc"
}
```
- Response:
```json
{
    "code": 201,
    "data": null,
    "status": "success"
}
```
